function fibonacci(b){
    if(n < 2) {
        return n;
    }
    return fibonacci(n - 1) + fibonacci(n - 2);
  }
function caching(fib) {
    let cache = new Map();
    return function(n) {
        if(cache.has(n)) {
            return cache.get(n);
        }
        let result = fib(n);
        cache.set(n, result);
        return result;
    }
}
let x = caching(fibonacci);
alert(x(11))