let arr = [2, 7, 3, 6, 8, 5, -10]
function selectionSort(arrSorted) {

    for (let i = 0; i < arrSorted.length; i++) {

        let min = i;

        for (let j = i + 1; j < arrSorted.length; j++) {
            if ( arrSorted[min] > arrSorted[j] ) {
                min = j;
            }
        }

        if ( i != min ) {
            [ arrSorted[i], arrSorted[min] ] = [arrSorted[min], arrSorted[i]];
        }
    }
    return arrSorted;
}

console.log(selectionSort(arr));